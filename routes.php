<?php

use Cms\Helpers\File as FileHelper;
use NetSTI\Editor\Models\Settings;
use Cms\Classes\ComponentPartial;
use Cms\Classes\MediaLibrary;

Route::get('/netsti/editor/images', function () {
	if (checkContentEditor()) {

		$path = Settings::get('image_folder', 'editor');
		$path = MediaLibrary::validatePath($path);

		$media = MediaLibrary::instance();
		$files = $media->listFolderContents($path, 'lastModified', 'image', true);

		$images = array();

		foreach ($files as $key => $value) {
			$images[] = [
				'url' => $value->publicUrl,
				'thumb' => $value->publicUrl
			];
		}

		return $images;
	}
});

Route::post('/netsti/editor/image/upload', function () {

	if (checkContentEditor()) {

		try {
			if (!Input::hasFile('image')) {
				throw new ApplicationException('File missing from request');
			}

			$uploadedFile = Input::file('image');
			$fileName = $uploadedFile->getClientOriginalName();

			 // Convert uppcare case file extensions to lower case
			$extension = strtolower($uploadedFile->getClientOriginalExtension());
			$fileName = File::name($fileName).'.'.$extension;

			 // File name contains non-latin characters, attempt to slug the value
			if (!FileHelper::validateName($fileName)) {
				$fileNameSlug = Str::slug(File::name($fileName), '-');
				$fileName = $fileNameSlug.'.'.$extension;
			}
			if (!$uploadedFile->isValid()) {
				throw new ApplicationException($uploadedFile->getErrorMessage());
			}

			$path = Settings::get('image_folder', 'editor');
			$path = MediaLibrary::validatePath($path);

			MediaLibrary::instance()->put(
				$path.'/'.$fileName,
				File::get($uploadedFile->getRealPath())
			);

			list($width, $height) = getimagesize($uploadedFile);

			return Response::json([
				'link'      => url('/storage/app/media'.$path.'/'.$fileName),
				'filename' => $fileName,
				'size'     => [
					$width,
					$height
				]
			]);
		}
		catch (Exception $ex) {
			return $ex;
		}

	}

});

Route::post('/netsti/editor/image/replace', function () {
	try {
		if (!Input::hasFile('image'))
			throw new ApplicationException('File missing from request');

		$uploadedFile = Input::file('image');

		if (checkContentEditor()) {
			$data = Input::all();
			$model_id = $data['model_id'];
			$attribute = $data['attribute'];
			$model = explode('.', $data['model']);
			$model = '\\'.$model[0].'\\'.$model[1].'\Models\\'.$model[2];

			

			if(class_exists($model) && $model = $model::find($model_id)){
				$model->$attribute = $uploadedFile;
				$model->save();
			}

			$fileName = $uploadedFile->getClientOriginalName();
			list($width, $height) = getimagesize($uploadedFile);

			return Response::json([
				'link'      => $model->$attribute->getPath(),
				'filename' => $fileName,
				'size'     => [
					$width,
					$height
				]
			]);
		}
	} catch (Exception $ex) {
		return $ex;
	}

});

Route::any('/netsti/editor/image/delete', function(){
	if (checkContentEditor()) {
		$path = MediaLibrary::validatePath(Input::get('src'));

		if($path){
			$media = MediaLibrary::instance();
			$pathEx = explode('media/', $path)[1];
			$media->deleteFiles([$pathEx]);
			// Storage::delete($path);

			return $path;
		}else{
			return ['Please enter src param'];
		}
	}
});

function checkContentEditor()
{
	$backendUser = BackendAuth::getUser();
	return $backendUser && $backendUser->hasAccess(Settings::get('permissions', 'cms.manage_content'));
}
