<?php namespace NetSTI\Editor\Classes;

use File;
use Markdown;
use Cms\Classes\Theme;
use Cms\Classes\Layout;
use ApplicationException;
use Cms\Classes\CmsCompoundObject;

class Block extends CmsCompoundObject
{
	protected $dirName = 'blocks';

	protected $fillable = [
        'title',
        'description',
        'icon',
        'image',
        'code',
        'markup'
    ];

    public $apiBag = [];

	protected $allowedExtensions = ['htm'];

	public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

	protected function parseSettings()
    {
    }

    public function getCodeClassParent()
    {
        return 'NetSTI\Editor\Classes\BlockCode';
    }

}
