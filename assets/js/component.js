;!(function ($) {
	$.fn.classes = function (callback) {
		var classes = [];
		$.each(this, function (i, v) {
			var splitClassName = v.className.split(/\s+/);
			for (var j = 0; j < splitClassName.length; j++) {
				var className = splitClassName[j];
				if (-1 === classes.indexOf(className)) {
					classes.push(className);
				}
			}
		});
		if ('function' === typeof callback) {
			for (var i in classes) {
				callback(classes[i]);
			}
		}
		return classes;
	};
})(jQuery);

$(function(){
	if(! $('#editor--alert').length)
		$('body').append('<p id="editor--alert" class="fade"></p>');

	if(! $('#editor--buttons').length)
		$('body').append('<div id="editor--buttons">'+
			'<a id="saveButton"></a>'+
			'<a id="loadingButton"></a>'+
			'<a id="closeButton"></a>'+
			'<a id="editButton"></a>'+
			// '<div class="addButtons"><a id="triggerAddButton"><i class="fa fa-plus"></i></a>'+
			// 	'<a data-request="onCreateElement" data-request-data="type:\'page\'" class="editor--create"><i class="fa fa-files-o"></i></a>'+
			// 	'<a data-request="onCreateElement" data-request-data="type:\'post\'" class="editor--create"><i class="fa fa-newspaper-o"></i></a>'+
			// '</div>'+
		'</div>');

	if(! $('#editor--subtoolbar').length)
		$('body').append('<div id="editor--subtoolbar"><div class="fr-tags"></div><div id="frClasses" /><div class="fr-clear-all"><i class="fa fa-trash"></i></div></div>');

	if(! $('#editor--blocks-toolbar').length)
		$('body').append('<div id="editor--blocks-toolbar" />');

	if(! $('#editor--image-editor').length)
		$('body').append('<div id="editor--image-editor" />');

	$.request('onGetRootPath', {
		success: function(data){
			$('body').attr('data-app', data.result);
		}
	});

	$('#editButton').click(function(event) {
		init($('body').data('app'));
	});

	$('.addButtons').click(function(event) {
		$(this).toggleClass('hover')
	});
});

function init(path){
	$('#editor--buttons').addClass('edit');
	$('body').addClass('editor--editable-mode');

	var allFonts = {}, renderedFonts = styleInPage('fontFamily'), rootPath = $('body').attr('data-app');

	var taggle = new Taggle('frClasses', {
		duplicateTagClass: 'taggle_bounce',
		onTagAdd: function(event, tag) {
			$('[data-focused="yes"]').addClass(tag);
		},
		onTagRemove: function(event, tag) {
			$('[data-focused="yes"]').removeClass(tag);
		}
	});

	var options = {
		"indent": true,
		"indent-spaces": 4,
		"wrap": 0,
		"markup": true,
		"output-xml": false,
		"numeric-entities": true,
		"quote-marks": true,
		"quote-nbsp": false,
		"show-body-only": true,
		"quote-ampersand": false,
		"break-before-br": true,
		"uppercase-tags": false,
		"uppercase-attributes": false,
		"drop-font-tags": true,
		"tidy-mark": false
	};

	// STYLES EDITOR
	$.FroalaEditor.DefineIcon('buttonIconClass', { NAME: 'tags' });
	$.FroalaEditor.DefineIcon('buttonIconBlocks', { NAME: 'cubes' });
	$.FroalaEditor.DefineIcon('buttonImageEditor', {NAME: 'magic'});

	$.FroalaEditor.RegisterCommand('imageEditor', {
		title: 'Image Editor',
		icon: 'buttonImageEditor',
		focus: false,
		undo: false,
		refreshAfterCallback: false,
		callback: function () {
			editImage(this.image.get());
			$('body').addClass('editor--image-editor-active');
		}
	});

	$.FroalaEditor.RegisterCommand('blocksDeck', {
		title: 'Blocks',
		icon: 'buttonIconBlocks',

		undo: true,
		focus: true,
		showOnMobile: false,
		refreshAfterCallback: true,

		callback: function() {
			$('#editor--blocks-toolbar').toggleClass('open');
			$('#editor--subtoolbar').removeClass('open');
		}
	});

	$.FroalaEditor.RegisterCommand('classEditor', {
		title: 'Tag Classes',
		icon: 'buttonIconClass',

		undo: false,
		focus: true,
		showOnMobile: false,
		refreshAfterCallback: true,

		callback: function() {
			$('#editor--subtoolbar').toggleClass('open');
			$('#editor--blocks-toolbar').removeClass('open');
		},

		refresh: function($btn) {
			$('[data-focused]').attr('data-focused', 'no');
			taggle.removeAll();

			$(this.selection.element()).attr('data-focused', 'yes');
			taggle.add($(this.selection.element()).classes());
			
			$('.fr-tags').html('');
			$('.fr-tags').prepend('<div class="fr-tag">'+ $(this.selection.element()).prop("tagName").toLowerCase() +'</div>');

			$(this.selection.element()).parentsUntil('.fr-element').each(function(index, el) {
				$('.fr-tags').prepend('<div class="fr-tag">'+ $(el).prop("tagName").toLowerCase() +'</div>')
			});
		}
	});

	$.request('onGetBlocksPartial');

	// $(renderedFonts).each(function(index, el) {
	// 	var fontFamily = el.replace(/"/g , '\'');
	// 	var nameFontFamily = fontFamily.substr(0, fontFamily.indexOf(',') || null).replace(/'/g, "");
	// 	console.log(fontFamily);
	// 	if(nameFontFamily)
	// 		allFonts[fontFamily] = nameFontFamily;
	// });

	$('[data-popup-attribute]').froalaEditor({
		editInPopup: true,
	}).on('froalaEditor.contentChanged', function (e, editor) {
		$(this).addClass('fr-changed');
		$('#editor--buttons').addClass('active');
	});

	$('[data-image-attribute]').each(function(index, el) {
		$('[data-image-attribute]').froalaEditor({
			imageEditButtons: [ 'imageReplace', 'imageEditor' ],
			imageUploadParam: 'image',
			imageUploadURL: path + '/netsti/editor/image/replace',
			imageUploadMethod: 'POST',
			imageUploadParams: {
				attribute: $(this).data('image-attribute'),
				model: $(this).data('image-model'),
				model_id: $(this).data('image-model-id')
			},
			imageInsertButtons: ['imageBack', '|', 'imageUpload'],
			imageMaxSize: 2 * 1024 * 1024,
			imageAllowedTypes: ['jpeg', 'jpg', 'png', 'gif'],
			imageManagerLoadMethod: 'GET',
			imageManagerLoadURL: path + '/netsti/editor/images'
		});
	});

	var editor = $('[data-editable], [data-editable-attribute]').froalaEditor({
		theme: 'dark',
		toolbarBottom: true,
		toolbarContainer: 'body',
		toolbarVisibleWithoutSelection: true,

		imageUploadParam: 'image',
		imageUploadURL: path + '/netsti/editor/image/upload',
		imageUploadMethod: 'POST',
		imageMaxSize: 2 * 1024 * 1024,
		imageAllowedTypes: ['jpeg', 'jpg', 'png', 'gif'],
		imageManagerLoadMethod: 'GET',
		imageManagerLoadURL: path + '/netsti/editor/images',
		imageManagerDeleteURL: path + '/netsti/editor/image/delete',

		// fontFamily: allFonts,
		// fontFamilySelection: true,
		fontSizeSelection: true,
        paragraphFormatSelection: true,

		imageEditButtons: [
			'imageReplace',
			'imageAlign',
			'imageEditor',

			'imageLink',
			'linkOpen', 
			'linkEdit', 
			'linkRemove',

			'imageAlt', 
			'imageSize',
			'imageRemove',
		],
		toolbarButtons: [
			'bold', 
			'italic', 
			'underline', 
			'|', 
			'paragraphFormat',
			// 'fontFamily', 
			'fontSize', 
			'color', 
			'emoticons', 
			'|', 
			'align', 
			'formatOL', 
			'formatUL', 
			'|',
			'quote',
			'insertImage', 
			'insertVideo', 
			'insertLink', 
			'insertHR',
			'undo', 
			'redo',
			'|',
			'classEditor',
			'blocksDeck',
			// 'html'
		]
	}).on('froalaEditor.drop', function (e, editor, dropEvent) {
		var blockContent = $('#'+dropEvent.originalEvent.dataTransfer.getData('Text'));

		// Focus at the current posisiton.
		editor.markers.insertAtPoint(dropEvent.originalEvent);
		var $marker = editor.$el.find('.fr-marker');
		$marker.replaceWith($.FroalaEditor.MARKERS);
		editor.selection.restore();

		var content = blockContent.find('.editor--block-content').html();

		// Insert HTML
		editor.html.insert('<!-- block -->'+content);

		checkBlocks();

		dropEvent.preventDefault();
		dropEvent.stopPropagation();
		editor.undo.saveStep();

		return false;
	}).on('froalaEditor.contentChanged', function (e, editor) {
		$(this).addClass('fr-changed');
		$('#editor--buttons').addClass('active');
	}).on('froalaEditor.image.error', function (e, editor, error, response) {
		console.log(error);
	});

	$('.fr-clear-all').click(function(event) {
		taggle.removeAll();
	});

	$('#closeButton').click(function(event) {
		$('#editor--buttons').removeClass('edit active loading');
		$('#editor--blocks-toolbar').removeClass('open');
		$('#editor--subtoolbar').removeClass('open');
		$('.fr-toolbar').addClass('fadeOutToolbar');

		setTimeout(function() {
			$('body').removeClass('editor--editable-mode');
			editor.froalaEditor('destroy');
			$('[data-editable]').addClass('fr-view');
		}, 400);
	});

	$('#saveButton').click (function () {
		var changes = [];

		$('[data-editable].fr-changed').each(function(index, el) {
			$('#editor--buttons').addClass('loading');
			$('[data-focused]').removeAttr('data-focused');

			changes.push({
				type: 'content',
				file: $(this).data('file'),
				content: tidy_html5($(this).froalaEditor('html.get', false), options)
			});
		});

		$('[data-editable-attribute].fr-changed').each(function(index, el) {
			$('#editor--buttons').addClass('loading');
			$('[data-focused]').removeAttr('data-focused');

			changes.push({
				id: $(this).data('editable-model-id'),
				type: 'attribute',
				attribute: $(this).data('editable-attribute'),
				model: $(this).data('editable-model'),
				content: tidy_html5($(this).froalaEditor('html.get', false), options)
			});
		});


		$('[data-popup-attribute].fr-changed').each(function(index, el) {
			$('#editor--buttons').addClass('loading');
			$('[data-focused]').removeAttr('data-focused');

			changes.push({
				id: $(this).data('editable-model-id'),
				type: 'attribute',
				attribute: $(this).data('popup-attribute'),
				model: $(this).data('editable-model'),
				content: $(this).html()
			});
		});

		$.request('onEditorSave', {
			data: {
				changes: changes
			},
			success: function(data) {
				console.info(data);
				$('#editor--buttons').removeClass('active loading'); 
				showAlert('success');
			},
			error: function(data) {
				console.error(data);
				$('#editor--buttons').removeClass('loading'); 
				showAlert('error');
			}
		});
	});

	checkBlocks();
}

/* FUNCTIONS */
function checkBlocks(){
	$('[data-template-append]').each(function(index, el) {
		$(el).click(function(e) {
			if (e.offsetY > (el.offsetHeight))
				$(el).append($(el).data('template-append'));
		});
	});

	$('[data-template-prepend]').each(function(index, el) {
		$(el).after().click(function(e) {
			if (e.offsetY < 0)
				$(el).prepend($(el).data('template-prepend'));
		});
	});
}

function showAlert (type){
	if(type == 'error'){
		var message = "Error on save content, try again later.",
		icon = 'fa fa-ban';
	}

	if(type == 'success'){
		var message = "The content was successfully saved.",
		icon = 'fa fa-check';
	}

	$('#editor--alert').addClass(type+' in').html('<i class="flash-icon '+icon+'"></i> '+message);

	setTimeout(function() { $('#editor--alert').removeClass('in') }, 5000);
};

function styleInPage(css, verbose){
	if(typeof getComputedStyle== "undefined")
	getComputedStyle= function(elem){
		return elem.currentStyle;
	}
	var who, hoo, values= [], val,
	nodes= document.body.getElementsByTagName('*'),
	L= nodes.length;
	for(var i= 0; i<L; i++){
		who= nodes[i];
		if(who.style){
			hoo= '#'+(who.id || who.nodeName+'('+i+')');
			val= who.style.fontFamily || getComputedStyle(who, '')[css];
			if(val){
				if(verbose) values.push([hoo, val]);
				else if(values.indexOf(val)== -1) values.push(val);
			}
			val_before = getComputedStyle(who, ':before')[css];
			if(val_before){
				if(verbose) values.push([hoo, val_before]);
				else if(values.indexOf(val_before)== -1) values.push(val_before);
			}
			val_after= getComputedStyle(who, ':after')[css];
			if(val_after){
				if(verbose) values.push([hoo, val_after]);
				else if(values.indexOf(val_after)== -1) values.push(val_after);
			}
		}
	}
	return values;
}

function editImage(image){
	var imageRender = new Image(),
	appPath = $('body').data('app');
	imageRender.src = image.attr('src');

	var kit = new ImglyKit({
		renderer: 'webgl',
		assetsUrl: appPath + '/plugins/netsti/editor/assets',
		container: document.querySelector('#editor--image-editor'),
		versionCheck: false,
		image: imageRender,
		ui: {
			enabled: true,
			showExportButton: false,
			showWebcamButton: false,
			export: {
				type: ImglyKit.ImageFormat.JPEG
			}
		},
		renderOnWindowResize: true
	});

	kit.ui.selectOperations('filters,rotation,flip, brightness,contrast, saturation,crop,text,brush,radial-blur,tilt-shift');

	kit.run();

	var imagePath = appPath + '/plugins/netsti/editor/assets/ui/night/top';

	$('.imglykit-top-controls-left').prepend(
		'<div id="editor--image-editor-save" class="imglykit-btn"><img src="'+imagePath+'/insert.png" alt="" /> Insert</div>'+
		'<div id="editor--image-editor-close" class="imglykit-btn"><img src="'+imagePath+'/cancel.png" alt="" /> Cancel</div>'
	);

	$('#editor--image-editor-save').on('click', function (e) {
		e.preventDefault();

		kit.render('image', 'image/png').then(function (imageTrans) {
			$(image).attr('src', imageTrans.src);
			$('body').removeClass('editor--image-editor-active');
		});
	});

	$('#editor--image-editor-close').on('click', function (e) {
		$('body').removeClass('editor--image-editor-active');
	});

	window.kit = kit;
}
