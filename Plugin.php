<?php namespace NetSTI\Editor;

use App;
use Event;
use Backend;
use Cms\Classes\Theme;
use System\Classes\PluginBase;

/**
 * Editor Plugin Information File
 */
class Plugin extends PluginBase
{

	/**
	* Returns information about this plugin.
	*
	* @return array
	*/
	public function pluginDetails(){
		return [
			'name'        => 'Content Editor',
			'description' => 'Front-end content editor',
			'author'      => 'NetSTI',
			'icon'        => 'icon-edit'
		];
	}

	public function registerComponents(){
		return [
			'NetSTI\Editor\Components\Editor' => 'editor',
		];
	}

	public function registerSettings(){
		return [
			// 'settings' => [
			// 	'label'       => 'Content Editor Settings',
			// 	'description' => 'Manage main editor settings.',
			// 	'icon'        => 'icon-edit',
			// 	'class'       => 'NetSTI\Editor\Models\Settings',
			// 	'order'       => 500,
			// 	'permissions' => ['netsti.editor.access_settings']
			// ]
		];
	}

	public function register(){
		Event::listen('backend.form.extendFields', function($widget){
			if (!$widget->model instanceof \Cms\Classes\Page) return;

			if (!($theme = Theme::getEditTheme()))
				throw new ApplicationException(Lang::get('cms::lang.theme.edit.not_found'));

			$widget->addSecondaryTabFields([
				'previewpage' => [
					'type'    => 'partial',
					'tab'     => 'Editor View',
					'path'    => '$/netsti/editor/partials/_editor.htm',
					'stretch' => true
				]
			]);
		});
	}
}
