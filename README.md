##Super awesome content editor from front-end.

### Features
- Edit content files with Froala Editor
- Multilingual support (Rainlab Translate needed)
- Image upload to MediaLibrary
- Advanced image editor
- Autocreate file if not exists
- Auto Fonts discovery
- Emoticons
- Add style class for selected element
- Drag & Drop Blocks of static code
- Preview and Edit from BackEnd CMS editor
- Responsive Preview Controls/Fullscreen from BackEnd CMS Editor 

### Coming Soon
- RainLab Pages frontend editor & creator
- RainLab Blog frontend editor & creator
- NetSTI Articles frontend editor & creator
- Blocks delete/internal templating/properties

### How to use it? It's simple. ###

* Drop the Content Editor component to a CMS page or add a global component in your template.
* Check if you have `{% framework %}` and `{% scripts %}` inside layout for working ajax requests
* Use this code in your page code and link the editor to a content file or set name to autocreate new file

```
{% component 'editor' file="filename_in_content.htm" %}
```

###You can create Blocks of static code

Create a new folder *blocks* in the root of your theme and create .htm files like this example:

```
title = "Jumbotron"
description = "Boostrap 3 Block"
image = "assets/images/bootstrap.png" or icon="star" 
==
<div class="jumbotron">
	<div class="container">
		<h1>Hello, world!</h1>
		<p>Contents ...</p>
		<p>
			<a class="btn btn-primary btn-lg">Learn more</a>
		</p>
	</div>
</div>
```