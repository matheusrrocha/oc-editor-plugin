<?php namespace NetSTI\Editor\Components;

use Html;
use BackendAuth;
use Cms\Classes\Theme;
use Cms\Classes\Content;
use Cms\Classes\CmsObject;
use Cms\Classes\ComponentBase;
use NetSTI\Editor\Classes\Block;

use NetSTI\Editor\Models\Settings;

class Editor extends ComponentBase
{
	public $content;
	public $file;
	public $buttons;
	public $palettes;

	public function componentDetails()
	{
		return [
			'name'        => 'Content Editor',
			'description' => 'Edit your front-end content in page.'
		];
	}

	public function defineProperties()
	{
		return [
			'file' => [
				'title'       => 'Content file',
				'description' => 'Content block filename to edit, optional',
				'default'     => '',
				'type'        => 'dropdown',
			]
		];
	}

	public function getFileOptions()
	{
		return Content::sortBy('baseFileName')->lists('baseFileName', 'fileName');
	}

	public function onRun()
	{
		if ($this->checkCanEdit()) {

			$this->addCss('assets/css/build.min.css');
			$this->addCss('assets/css/tingle.min.css');
			$this->addCss('assets/css/font-awesome.min.css');
			$this->addCss('assets/css/component.css');

			$this->addJs('assets/js/tidy.min.js');
			$this->addJs('assets/js/tingle.min.js');
			$this->addJs('assets/js/editor.min.js');
			$this->addJs('assets/js/combined.min.js');
			$this->addJs('assets/js/pagelinks.js');
			$this->addJs('assets/js/imgeditor.min.js');
			$this->addJs('assets/js/component.js');
		}else{
			$this->addCss('assets/css/build.min.css');
		}
	}

	public function onRender()
	{

		$this->file = $this->property('file');

		// Compatability with RainLab.Translate
		if (class_exists('\RainLab\Translate\Classes\Translator')) {
			$locale = \RainLab\Translate\Classes\Translator::instance()->getLocale();
			$fileName = substr_replace($this->file, '.'.$locale, strrpos($this->file, '.'), 0);
			if (($content = Content::loadCached($this->page->controller->getTheme(), $fileName)) !== null)
				$this->file = $fileName;
		}

		if ($this->checkCanEdit()) {
			if (Content::load($this->getTheme(), $this->file))
				$this->content = $this->renderContent($this->file);
			else
				$this->content = '';
		} else {
			if (!Content::load($this->getTheme(), $this->file)) {
				$fileContent = Content::inTheme($this->getTheme());
				$fileContent->fill([
					'fileName' => $this->file,
					'markup' => post('content')
				]);
				$fileContent->save();
			}

			return $this->renderContent($this->file);
		}
	}

	public function onEditorSave()
	{
		if ($this->checkCanEdit() && count(post('changes'))) {
			$changes = post('changes');

			foreach ($changes as $change) {
				switch ($change['type']):
				case 'content':
					$fileName = $change['file'];
					$content  = $change['content'];

					if (class_exists('\RainLab\Translate\Classes\Translator')) {
						$locale = \RainLab\Translate\Classes\Translator::instance()->getLocale();
						$defaultLocale = \RainLab\Translate\Classes\Translator::instance()->getDefaultLocale();

						if(!strrpos($fileName, '.'))
							$fileName .= '.htm'; 

						if($defaultLocale != $locale)
							$fileName = substr_replace($fileName, '.'.$locale, strrpos($fileName, '.'), 0);
					}

					if ($load = Content::load($this->getTheme(), $fileName)) {
						$fileContent = $load;
					} else {
						$fileContent = Content::inTheme($this->getTheme());
					}

					$fileContent->fill([
						'fileName' => $fileName,
						'markup' => $content
					]);

					$fileContent->save();
				break;

				case 'attribute':
					$model_id = $change['id'];
					$attribute = $change['attribute'];
					$model = explode('.', $change['model']);
					$content  = $change['content'];
					$model = '\\'.$model[0].'\\'.$model[1].'\Models\\'.$model[2];

					if(class_exists($model) && $model = $model::find($model_id)){
						$model->$attribute = $content;
						$model->save();
					}

					return [$attribute, $model, $model_id, $content];
				break;

				endswitch;
 			}
		}
	}

	public function checkCanEdit()
	{
		$backendUser = BackendAuth::getUser();
		return $backendUser && $backendUser->hasAccess(Settings::get('permissions', 'cms.manage_content'));
	}

	public function onGetRootPath(){
		return url('/');
	}

	public function onGetBlocksPartial(){
		$theme = Theme::getActiveTheme();
		$blocks = Block::listInTheme($theme, true);

		return ['#editor--blocks-toolbar' => $this->renderPartial('@blocks', ['blocks' => $blocks])];
	}

	public function onCreateElement(){
		throw new \ApplicationException("This feature comming soon", 1);
	}
}
